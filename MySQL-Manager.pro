#-------------------------------------------------
#
# Project created by QtCreator 2017-05-03T21:06:48
#
#-------------------------------------------------

QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MySQL-Manager
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp \
    mysql.cpp \
    listwidget.cpp \
    toolbar.cpp \
    tabwidget.cpp \
    formview.cpp \
    objectdesigner.cpp \
    formdesigner.cpp \
    querydesigner.cpp \
    reportdesigner.cpp \
    tableview.cpp

HEADERS  += mainwindow.h \
    dialog.h \
    mysql.h \
    listwidget.h \
    toolbar.h \
    tabwidget.h \
    formview.h \
    objectdesigner.h \
    formdesigner.h \
    querydesigner.h \
    reportdesigner.h \
    tableview.h

FORMS    += mainwindow.ui
