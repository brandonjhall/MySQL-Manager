#ifndef QUERYDESIGNER_H
#define QUERYDESIGNER_H

#include "objectdesigner.h"

class QueryDesigner : public ObjectDesigner
{
    Q_OBJECT
public:
    explicit QueryDesigner(QWidget *parent = 0);
};

#endif // QUERYDESIGNER_H
