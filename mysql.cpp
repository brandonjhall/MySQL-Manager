#include "mysql.h"

#include <QtSql/QSql>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>

#ifdef QT_DEBUG
#include <QDebug>
#endif

MySQL::MySQL(QObject *parent) : QObject(parent)
{
    dbms = QSqlDatabase::addDatabase("QMYSQL");
    query = new QSqlQuery;

    dbms.setHostName(hostname);
}

QString MySQL::getUsername() const
{
    return username;
}

void MySQL::setUsername(const QString &value)
{
    username = value;
    dbms.setUserName(value);
}

QString MySQL::getHostname() const
{
    return hostname;
}

void MySQL::setHostname(const QString &value)
{
    hostname = value;
    dbms.setHostName(value);
}

QString MySQL::getPassword() const
{
    return password;
}

void MySQL::setPassword(const QString &value)
{
    password = value;
    dbms.setPassword(value);
}

QString MySQL::getQuery() const
{
    return query->lastQuery();
}

void MySQL::setQuery(QString query_str)
{
    query->prepare(query_str);

#ifdef QT_DEBUG
    qDebug() << "Set current query:";
    qDebug() << (query->lastQuery() == query_str);
#endif
}

bool MySQL::connect()
{
    dbms.open();

#ifdef QT_DEBUG
    if(dbms.lastError().type() != QSqlError::NoError)
        qDebug() << dbms.lastError().text();
#endif

    if(dbms.isOpen())
        return true;
    else
        return false;
}

bool MySQL::connect(QString username, QString hostname, QString password, QString database)
{
    dbms.setHostName(hostname);
    dbms.setUserName(username);
    dbms.setPassword(password);
    dbms.setDatabaseName(database);

    return connect();
}

bool MySQL::execQuery()
{
    if(query->exec())
    {

#ifdef QT_DEBUG
        while(query->next())
        {
            qDebug() << "Value: "
                     << query->value(0);
        }
#endif

        return true;
    }
    else
    {
        return false;
    }
}
