#include "mainwindow.h"
#include <QApplication>

#ifdef QT_DEBUG
#include "mysql.h"
#include <QDebug>
#endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.showMaximized();

#ifdef QT_DEBUG
    MySQL *mysql = new MySQL;

    mysql->setUsername("bjhall");
    mysql->setPassword("Ch_mist10");
    mysql->setHostname("SQLUbuntu");
    if(mysql->connect())
        mysql->setQuery("SHOW DATABASES");
    else
        qDebug() << "Could not connect.";

    mysql->execQuery();
#endif

    return a.exec();
}
