#ifndef REPORTDESIGNER_H
#define REPORTDESIGNER_H

#include "objectdesigner.h"

class ReportDesigner : public ObjectDesigner
{
    Q_OBJECT
public:
    explicit ReportDesigner(QWidget *parent = 0);
};

#endif // REPORTDESIGNER_H
