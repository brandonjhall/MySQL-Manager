#ifndef TOOLBAR_H
#define TOOLBAR_H

#include <QToolBar>

class ToolBar : public QToolBar
{
    Q_OBJECT
public:
    explicit ToolBar(QWidget *parent = nullptr);
    ToolBar(const QString title, QWidget *parent = nullptr);

signals:

public slots:
};

#endif // TOOLBAR_H
