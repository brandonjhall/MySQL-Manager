#ifndef OBJECTDESIGNER_H
#define OBJECTDESIGNER_H

#include <QWidget>

class ObjectDesigner : public QWidget
{
    Q_OBJECT
public:
    explicit ObjectDesigner(QWidget *parent = 0);

signals:

public slots:

protected:
    bool eventFilter(QObject *watched, QEvent *event);
};

#endif // OBJECTDESIGNER_H
