#ifndef MYSQL_H
#define MYSQL_H

#include <QObject>
#include <QtSql/QSqlDatabase>

class QSqlQuery;
class QSqlError;

class MySQL : public QObject
{
    Q_OBJECT
public:
    explicit MySQL(QObject *parent = 0);

    QString getUsername() const;
    void setUsername(const QString &value);

    QString getHostname() const;
    void setHostname(const QString &value);

    QString getPassword() const;
    void setPassword(const QString &value);

    QString getQuery() const;
    void setQuery(QString query_str);

    bool connect();
    bool connect(QString username , QString hostname = "localhost",
                 QString password = NULL, QString database = NULL);

    bool execQuery();

signals:

public slots:

private:
    QString username = "root";
    QString hostname = "localhost";
    QString password;
    QStringList databases;
    QSqlDatabase dbms;
    QSqlQuery *query;
    QSqlError *error;
};

#endif // MYSQL_H
