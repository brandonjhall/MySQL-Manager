#include "objectdesigner.h"
#include <QEvent>
#include <QKeyEvent>
#include <QPoint>
#include <QApplication>

#ifdef QT_DEBUG
#include <QDebug>
#endif

ObjectDesigner::ObjectDesigner(QWidget *parent) : QWidget(parent)
{
    QWidget *widget = new QWidget(this);

    widget->setStyleSheet("background: #FFFFFF");
    widget->installEventFilter(this);
}

bool ObjectDesigner::eventFilter(QObject *watched, QEvent *event)
{
    QWidget *widget = (QWidget*) watched;

    if(event->type() == QEvent::Enter)
    {
        QCursor overCursor;

        overCursor.setShape(Qt::OpenHandCursor);
        widget->setCursor(overCursor);
    }

    if(event->type() == QEvent::Leave)
    {
        QApplication::restoreOverrideCursor();
    }

    if(event->type() == QEvent::MouseButtonPress)
    {
        widget->setFocus();
    }

    if(event->type() == QEvent::MouseMove)
    {
#ifdef QT_DEBUG
        int wx, wy, cx, cy;

        wx = mapFromGlobal(widget->pos()).x();
        wy = mapFromGlobal(widget->pos()).y();
        cx = widget->parentWidget()->mapFromGlobal(QCursor::pos()).x();
        cy = widget->parentWidget()->mapFromGlobal(QCursor::pos()).y();

        qDebug() << wx << wy << cx << cy;
#endif

        widget->setCursor(QCursor(Qt::ClosedHandCursor));
        widget->move(widget->parentWidget()->mapFromGlobal(QCursor::pos()));
    }

    if(event->type() == QEvent::MouseButtonRelease)
    {
        widget->setCursor(QCursor(Qt::ArrowCursor));
    }

    if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent *key = (QKeyEvent*) event;
        QPoint point = widget->pos();
        int x = point.x();
        int y = point.y();

        if(key->key() == Qt::Key_Left)
        {
            x -= 5;
        }

        if(key->key() == Qt::Key_Up)
        {
            y -= 5;
        }

        if(key->key() == Qt::Key_Right)
        {
            x += 5;
        }

        if(key->key() == Qt::Key_Down)
        {
            y += 5;
        }

        widget->move(widget->parentWidget()->mapFromParent(QPoint(x, y)));
    }

    return QWidget::eventFilter(watched, event);
}
