#ifndef FORMDESIGNER_H
#define FORMDESIGNER_H

#include "objectdesigner.h"

class FormDesigner : public ObjectDesigner
{
    Q_OBJECT
public:
    explicit FormDesigner(QWidget *parent = 0);
};

#endif // FORMDESIGNER_H
